# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: d-i-manual_boot-new\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2022-05-19 22:02+0000\n"
"PO-Revision-Date: 2015-04-15 20:29+0000\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: Tagalog <LL@li.org>\n"
"Language: tl\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: boot-new.xml:5
#, no-c-format
msgid "Booting Into Your New &debian; System"
msgstr ""

#. Tag: title
#: boot-new.xml:7
#, no-c-format
msgid "The Moment of Truth"
msgstr ""

#. Tag: para
#: boot-new.xml:8
#, no-c-format
msgid ""
"Your system's first boot on its own power is what electrical engineers call "
"the <quote>smoke test</quote>."
msgstr ""

#. Tag: para
#: boot-new.xml:13
#, no-c-format
msgid ""
"If you did a default installation, the first thing you should see when you "
"boot the system is the menu of the <classname>grub</classname> bootloader. "
"The first choices in the menu will be for your new &debian; system. If you "
"had any other operating systems on your computer (like Windows) that were "
"detected by the installation system, those will be listed lower down in the "
"menu."
msgstr ""

#. Tag: para
#: boot-new.xml:23
#, no-c-format
msgid ""
"If the system fails to start up correctly, don't panic. If the installation "
"was successful, chances are good that there is only a relatively minor "
"problem that is preventing the system from booting &debian;. In most cases "
"such problems can be fixed without having to repeat the installation. One "
"available option to fix boot problems is to use the installer's built-in "
"rescue mode (see <xref linkend=\"rescue\"/>)."
msgstr ""

#. Tag: para
#: boot-new.xml:32
#, no-c-format
msgid ""
"If you are new to &debian; and &arch-kernel;, you may need some help from "
"more experienced users. <phrase arch=\"x86\">For direct on-line help you can "
"try the IRC channels #debian or #debian-boot on the OFTC network. "
"Alternatively you can contact the <ulink url=\"&url-list-subscribe;\">debian-"
"user mailing list</ulink>.</phrase> <phrase arch=\"not-x86\">For less common "
"architectures like &arch-title;, your best option is to ask on the <ulink "
"url=\"&url-list-subscribe;\">debian-&arch-listname; mailing list</ulink>.</"
"phrase> You can also file an installation report as described in <xref "
"linkend=\"submit-bug\"/>. Please make sure that you describe your problem "
"clearly and include any messages that are displayed and may help others to "
"diagnose the issue."
msgstr ""

#. Tag: para
#: boot-new.xml:48
#, no-c-format
msgid ""
"If you had any other operating systems on your computer that were not "
"detected or not detected correctly, please file an installation report."
msgstr ""

#. Tag: title
#: boot-new.xml:150
#, no-c-format
msgid "Mounting encrypted volumes"
msgstr ""

#. Tag: para
#: boot-new.xml:152
#, no-c-format
msgid ""
"If you created encrypted volumes during the installation and assigned them "
"mount points, you will be asked to enter the passphrase for each of these "
"volumes during the boot."
msgstr ""

#. Tag: para
#: boot-new.xml:160
#, no-c-format
msgid ""
"For partitions encrypted using dm-crypt you will be shown the following "
"prompt during the boot: <informalexample><screen>\n"
"Starting early crypto disks... <replaceable>part</"
"replaceable>_crypt(starting)\n"
"Enter LUKS passphrase:\n"
"</screen></informalexample> In the first line of the prompt, "
"<replaceable>part</replaceable> is the name of the underlying partition, e."
"g. sda2 or md0. You are now probably wondering <emphasis>for which volume</"
"emphasis> you are actually entering the passphrase. Does it relate to your "
"<filename>/home</filename>? Or to <filename>/var</filename>? Of course, if "
"you have just one encrypted volume, this is easy and you can just enter the "
"passphrase you used when setting up this volume. If you set up more than one "
"encrypted volume during the installation, the notes you wrote down as the "
"last step in <xref linkend=\"partman-crypto\"/> come in handy. If you did "
"not make a note of the mapping between <filename><replaceable>part</"
"replaceable>_crypt</filename> and the mount points before, you can still "
"find it in <filename>/etc/crypttab</filename> and <filename>/etc/fstab</"
"filename> of your new system."
msgstr ""

#. Tag: para
#: boot-new.xml:183
#, no-c-format
msgid ""
"The prompt may look somewhat different when an encrypted root file system is "
"mounted. This depends on which initramfs generator was used to generate the "
"initrd used to boot the system. The example below is for an initrd generated "
"using <classname>initramfs-tools</classname>:"
msgstr ""

#. Tag: screen
#: boot-new.xml:190
#, no-c-format
msgid ""
"Begin: Mounting <emphasis>root file system</emphasis>... ...\n"
"Begin: Running /scripts/local-top ...\n"
"Enter LUKS passphrase:"
msgstr ""

#. Tag: para
#: boot-new.xml:192
#, no-c-format
msgid ""
"No characters (even asterisks) will be shown while entering the passphrase. "
"If you enter the wrong passphrase, you have two more tries to correct it. "
"After the third try the boot process will skip this volume and continue to "
"mount the next filesystem. Please see <xref linkend=\"crypto-troubleshooting"
"\"/> for further information."
msgstr ""

#. Tag: para
#: boot-new.xml:200
#, no-c-format
msgid "After entering all passphrases the boot should continue as usual."
msgstr ""

#. Tag: title
#: boot-new.xml:207
#, no-c-format
msgid "Troubleshooting"
msgstr ""

#. Tag: para
#: boot-new.xml:209
#, no-c-format
msgid ""
"If some of the encrypted volumes could not be mounted because a wrong "
"passphrase was entered, you will have to mount them manually after the boot. "
"There are several cases."
msgstr ""

#. Tag: para
#: boot-new.xml:218
#, no-c-format
msgid ""
"The first case concerns the root partition. When it is not mounted "
"correctly, the boot process will halt and you will have to reboot the "
"computer to try again."
msgstr ""

#. Tag: para
#: boot-new.xml:225
#, no-c-format
msgid ""
"The easiest case is for encrypted volumes holding data like <filename>/home</"
"filename> or <filename>/srv</filename>. You can simply mount them manually "
"after the boot."
msgstr ""

#. Tag: para
#: boot-new.xml:231
#, no-c-format
msgid ""
"However for dm-crypt this is a bit tricky. First you need to register the "
"volumes with <application>device mapper</application> by running: "
"<informalexample><screen>\n"
"<prompt>#</prompt> <userinput>/etc/init.d/cryptdisks start</userinput>\n"
"</screen></informalexample> This will scan all volumes mentioned in "
"<filename>/etc/crypttab</filename> and will create appropriate devices under "
"the <filename>/dev</filename> directory after entering the correct "
"passphrases. (Already registered volumes will be skipped, so you can repeat "
"this command several times without worrying.) After successful registration "
"you can simply mount the volumes the usual way:"
msgstr ""

#. Tag: screen
#: boot-new.xml:246
#, no-c-format
msgid ""
"<prompt>#</prompt> <userinput>mount <replaceable>/mount_point</replaceable></"
"userinput>"
msgstr ""

#. Tag: para
#: boot-new.xml:249
#, no-c-format
msgid ""
"If any volume holding noncritical system files could not be mounted "
"(<filename>/usr</filename> or <filename>/var</filename>), the system should "
"still boot and you should be able to mount the volumes manually like in the "
"previous case. However, you will also need to (re)start any services usually "
"running in your default runlevel because it is very likely that they were "
"not started. The easiest way is to just reboot the computer."
msgstr ""

#. Tag: title
#: boot-new.xml:267
#, no-c-format
msgid "Log In"
msgstr ""

#. Tag: para
#: boot-new.xml:269
#, no-c-format
msgid ""
"Once your system boots, you'll be presented with the login prompt. Log in "
"using the personal login and password you selected during the installation "
"process. Your system is now ready for use."
msgstr ""

#. Tag: para
#: boot-new.xml:275
#, no-c-format
msgid ""
"If you are a new user, you may want to explore the documentation which is "
"already installed on your system as you start to use it. There are currently "
"several documentation systems, work is proceeding on integrating the "
"different types of documentation. Here are a few starting points."
msgstr ""

#. Tag: para
#: boot-new.xml:283
#, no-c-format
msgid ""
"Documentation accompanying programs you have installed can be found in "
"<filename>/usr/share/doc/</filename>, under a subdirectory named after the "
"program (or, more precise, the &debian; package that contains the program). "
"However, more extensive documentation is often packaged separately in "
"special documentation packages that are mostly not installed by default. For "
"example, documentation about the package management tool <command>apt</"
"command> can be found in the packages <classname>apt-doc</classname> or "
"<classname>apt-howto</classname>."
msgstr ""

#. Tag: para
#: boot-new.xml:294
#, no-c-format
msgid ""
"In addition, there are some special folders within the <filename>/usr/share/"
"doc/</filename> hierarchy. Linux HOWTOs are installed in <emphasis>.gz</"
"emphasis> (compressed) format, in <filename>/usr/share/doc/HOWTO/en-txt/</"
"filename>. After installing <classname>dhelp</classname>, you will find a "
"browsable index of documentation in <filename>/usr/share/doc/HTML/index."
"html</filename>."
msgstr ""

#. Tag: para
#: boot-new.xml:303
#, no-c-format
msgid ""
"One easy way to view these documents using a text based browser is to enter "
"the following commands: <informalexample><screen>\n"
"$ cd /usr/share/doc/\n"
"$ w3m .\n"
"</screen></informalexample> The dot after the <command>w3m</command> command "
"tells it to show the contents of the current directory."
msgstr ""

#. Tag: para
#: boot-new.xml:313
#, no-c-format
msgid ""
"If you have a graphical desktop environment installed, you can also use its "
"web browser. Start the web browser from the application menu and enter "
"<userinput>/usr/share/doc/</userinput> in the address bar."
msgstr ""

#. Tag: para
#: boot-new.xml:319
#, no-c-format
msgid ""
"You can also type <userinput>info <replaceable>command</replaceable></"
"userinput> or <userinput>man <replaceable>command</replaceable></userinput> "
"to see documentation on most commands available at the command prompt. "
"Typing <userinput>help</userinput> will display help on shell commands. And "
"typing a command followed by <userinput>--help</userinput> will usually "
"display a short summary of the command's usage. If a command's results "
"scroll past the top of the screen, type <userinput>|&nbsp;more</userinput> "
"after the command to cause the results to pause before scrolling past the "
"top of the screen. To see a list of all commands available which begin with "
"a certain letter, type the letter and then two tabs."
msgstr ""
